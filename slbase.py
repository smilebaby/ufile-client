#!/usr/bin/python
# -*- coding: utf-8 -*-

r"""SmileLeaf Base Module
本模块会在 smilelleaf 的安装程序，执行程序中调用 ； 安装之前，之后 都可以调用 ，并判断出系统状态
"""
import sys
import os
import subprocess
import hashlib
import urllib2
import urlparse
import json
import socket

G_URL_ROOT_FORVER = "http://smileleaf.hgzp.com/smileleaf/"
G_APP_NAME ="smileleaf"

class SmileLeafBaseException(Exception):    pass
    
class FileNoFound(SmileLeafBaseException):  pass

class VersionLow(SmileLeafBaseException):
    pass
class VersionHigh(SmileLeafBaseException):
    pass
class NotFound(SmileLeafBaseException):
    pass
class AppInvalid(SmileLeafBaseException):
    pass

def is_port_open(port):
    """
    查看指定的端口是否已经打开
    """
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    try:
        s.connect(("127.0.0.1",int(port)))
        s.shutdown(2)
        return True
    except:
        return False    

def sp_run(cmd_line,shell=False):
    """利用 subprocess  调用其他程序，并获取其控制台输出
    参数
    cmd_line :  调用程序 及 命令行参数 列表
    返回
    执行返回码，stdout输出，stderr输出 
    """

    try:
        p=subprocess.Popen( 
                    cmd_line,
                    stdin=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    stdout=subprocess.PIPE,
                    shell=shell
                  )
        str_out,str_error=p.communicate()         #communicate 返回  stdout 和 stderr
    except WindowsError,e:
        if e.winerror==2: #not find
            raise NotFound
        raise e

    return p.returncode,str_out,str_error

def sp_run_nowait(cmd_line,shell=False):
    try:
        p=subprocess.Popen( 
                    cmd_line,
                    stderr=subprocess.STDOUT,
                    shell=shell
                  )
    except WindowsError,e:
        if e.winerror==2: #not find
            raise NotFound
        raise e

    return p

def sp_run_nowait_ex(cmd_line,shell=False):
    try:
        p=subprocess.Popen( 
                    cmd_line,
                    stdin=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    stdout=subprocess.PIPE,
                    shell=shell
                  )
    except WindowsError,e:
        if e.winerror==2: #not find
            raise NotFound
        raise e

    return p

class simple_dot_dict(dict):
    """
    source  : http://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute-in-python
    效果和下面的 dot_dict 一样，但是不能多层，即成员变量如果是 字典，还需如常访问  simple_dot_dict_inst.dict_member['key']
    访问简单，不需要下面的 v 函数
    """
    def __init__(self, *args, **kwargs):
        super(simple_dot_dict, self).__init__(*args, **kwargs)
        self.__dict__ = self    

def get_file_sha1(filepath):
    """
    获取文件的 sha1 码
    """
    calc_sha1_obj = hashlib.new('sha1')
    if os.path.exists(filepath):
        with open(filepath,"rb") as fhandle:
            calc_sha1_obj.update( fhandle.read() )
            return calc_sha1_obj.hexdigest()
    raise FileNoFound()


def create_folder_tree( folderpath ):
    if not os.path.exists(folderpath):
        os.makedirs(folderpath)



def c_python( want=[2,7,0] ):        
    """
    检查本机当前安装的 python 环境及期待的版本

    default need > 2.7
    test : c_python( want=[3,7,0] )

    check python
    """
    returncode,str_out,str_error = sp_run(  ["python","--version"] )
    content = str_error
    content = content.lower().strip()
    
    [name,version]=[x.strip() for x in content.split(" ")]
    [v0,v1,v2]=[int( x.strip() )  for x in version.split(".")]
    python_version = [v0,v1,v2]
    
    if name == "python":
        if v0 == want[0]:
            if v1>=want[1] and v2>=want[2]:
                return python_version
            else:
                raise VersionLow ()
        elif v0>want[0]:
            raise VersionHigh()
        else:
            raise VersionLow()
    
    raise NotFound()

def c_smileleaf(app):
    """
    app 读入的 app path 
    """
    exepath  =  os.path.join( app,"smileleaf.exe" )
    if not os.path.exists(exepath):
        raise NotFound()
    returncode,str_out,str_error = sp_run(  [exepath,"--miniversion"] )
    content = str_error
    [v0,v1,v2]=[int( x.strip() )  for x in content.split(".")]
    smileleaf_version = [v0,v1,v2]
    return smileleaf_version


def make_shortcut(app):
    """
    app 读入的 app path 
    """
    exepath  =  os.path.join( app,"smileleaf.exe" )
    if not os.path.exists(exepath):
        raise NotFound()
    returncode,str_out,str_error = sp_run(  [exepath,"--shortcut"] )
    return str_out


class FolderConfig:
    """
    smlieleaf  全部文件夹信息及，主要配置文件

    由系统及安装程序确定，并计入 sl.json
    pchome,pc,user 对全部用户一致，并在 smileleaf 安装前，也可使用
    
    pchome    "C:\ProgramData\smileleaf"
    pc        "C:\ProgramData\smileleaf\.system"
    use       "C:\ProgramData\smileleaf\.user"

    安装程序确定，并计入 sl.json
    app       "D:\smileleaf"
              "C:\Program Files\smileleaf"

    smileleaf_version
    python_version
    config
    """
    
    def __init__(self, install=False):
        """
        install : 是否是从安装程序发起的调用
        """
        self.appname =  G_APP_NAME
        self.pchome =   None
        self.pc  =      None
        self.user  =    None
        self.win_init()

        self.app =      None
        if not install:
            #普通调用
            self.app_init()

    def win_init(self):
        if not  self.pchome:
            win_alluserprofile =  os.environ.get( 'ALLUSERSPROFILE', "c:\\alluser" )
        
            self.pchome = os.path.join( win_alluserprofile , self.appname) 
            self.pc  =  os.path.join( self.pchome , ".system")
            self.user  =  os.path.join( self.pchome , ".user")

            create_folder_tree(self.pc)
            create_folder_tree(self.user)

    def __str__(self):
        return  "\n".join( [ self.pchome,self.app ] )  #, "%s"%self.smileleaf_version, "%s"%self.python_version ] )

    
    def pc_folder(self, subfolder,ifcreate=False):
        f = os.path.join( self.pc ,subfolder )
        if ifcreate:
            create_folder_tree(f)
        return f

    def user_folder(self,userid,subfolder,ifcreate=False):
        f = os.path.join( self.user, userid ,subfolder )
        if ifcreate:
            create_folder_tree(f)
        return f

    def app_folder(self,subfolder,ifcreate=False):
        if not self.app:
            raise AppInvalid()
            
        f = os.path.join( self.app ,subfolder )
        if ifcreate:
            create_folder_tree(f)
        return f

    def user_folder_ex(self,userid,*subfolder):
        f = os.path.join( self.user, userid ,*subfolder )
        return f
    def app_folder_ex(self,*subfolder):
        if not self.app:
            raise AppInvalid()
        f = os.path.join( self.app ,*subfolder )
        return f



    def install_app(self,ifcreate=True):
        """
        安装程序
        期望安装在 D 盘根目录下，但如 D 盘不存在，则安装到 program file
        """
        r = "D:\\"
        if not os.path.exists(r):
            r =os.environ["PROGRAMFILES"]
            assert( os.path.exists(r) )

        app = os.path.join( r ,"smileleaf" )  # 此时 还未正确安装，不宜设置 self.app

        if ifcreate:
            create_folder_tree(app)

        return app
        
    def read_conf(self):
        conf_path = os.path.join( self.pc , "sl.json" )
        if os.path.exists( conf_path ):
            with open (conf_path,"r") as f:
                return json.loads( f.read() ) 
        else:
            conf={}
            conf["folder"]={}

            conf["folder"]["pchome"]    = self.pchome
            conf["folder"]["app"] , exefile      = os.path.split( sys.executable )

            if exefile.lower() == "python.exe":
                conf["folder"]["app"] , _      = os.path.split(__file__)

            self.write_conf(conf)
            self.app= conf["folder"]["app"]
            return conf
        
        #raise AppInvalid()


    def write_conf(self,conf):
        conf_path = os.path.join( self.pc , "sl.json" )
        with open (conf_path,"w") as f:
            f.write( json.dumps(conf,indent=4 ) )

    def app_init(self):
        """
        正常读取配置信息
        """
        conf = self.read_conf()
        assert self.pchome == conf["folder"]["pchome"]
        self.app = conf["folder"]["app"]



class RemoteFileLibrary:
    """
    远程文件库管理

    文件夹   ： \\192.168.50.2\c$\nginx-1.4.1_80\html\smileleaf\file
    URL     ： http://smileleaf.hgzp.com/smileleaf/file

    文件结构

    远程文件库全局信息 index.json
        {
            "install":{
                "smileleaf":"file/smileleaf/index.json",
                "orgpython2":"file/orgpython2/index.json"
                }
        }
        
    每个文件包一个目录，比如 smileleaf
    http://smileleaf.hgzp.com/smileleaf/file/smileleaf
    其信息为
    http://smileleaf.hgzp.com/smileleaf/file/smileleaf/index.json

    {
        "current":
        {
            "id":"2_7_9",
            "version":"2.7.9",
            "filename":"python-2.7.9.msi",
            "date":"2014-12-10",
            "length":18309120,
            "sha1":"719832e0159eebf9cd48104c7db49aa978f6156c"
        },
    
        "history":[
            {
                "id_2_7_9":{
                    "id":"2_7._9",
                    "version":"2.7.9",
                    "filename":"python-2.7.9.msi",
                    "date":"2014-12-10",
                    "length":18309120,
                    "sha1":"719832e0159eebf9cd48104c7db49aa978f6156c"
                }
            }
    
        ]

    }



    """

    def __init__(self):
        self.info_root = self._down_json( "file/index.json" )

    def _down_json(self,url):
        url = urlparse.urljoin( G_URL_ROOT_FORVER ,url)
        #print url
        json_data =  urllib2.urlopen( url , timeout=10).read()
        data= json.loads( json_data)
        return data

    def get_info( self,softwarename):
        assert softwarename
        info_software = self._down_json( self.info_root["install"][softwarename] )["current"]
        fileurl=urlparse.urljoin(   G_URL_ROOT_FORVER,  self.info_root["install"][softwarename]  )
        fileurl=urlparse.urljoin(   fileurl,            info_software["filename"]  )

        info_software["url"] = fileurl
        if not info_software.has_key("sha1"):
            info_software["sha1"]=None
        return simple_dot_dict(info_software)



if __name__=="__main__":
    #fc = FolderConfig()
    #test1
    #print folder_user_home()
    #print folder_user_home('smileleaf')
    #create_folder_tree(  os.path.join(folder_user_home('smileleaf'),"a\\b\\c\\1\\2\\3")  )
    #print get_file_sha1('f:/e5ecacce05e44034438e60c5bb81255e1e97c0ce')
    print get_file_sha1('d:/da39a3ee5e6b4b0d3255bfef95601890afd80709.tiff')

