#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
gir test
'''

UPLOAD_MODULE = ''  # r:requests (default)  c:Curl

import hashlib
import urllib2
import StringIO
import sys
import os
import json
import time

hiapi_password='7f3b311ecc4e8b47c14a9cb45ae639cd'
admin_password='5c51aa76ac03474b197493922f25ab6b'

import slbase
import writeidhtml

up_url_template     = "http://{0}/ufile/up/{1}"

up8888_url_template     = "http://{0}/ufile/up/{1}?hiapi_password={2}"

up_url_template     = "http://{0}/ufile/up/{1}?temp=1"

down_url_template   = "http://{0}/ufile/down/{1}"
info_url_template   = "http://{0}/ufile/api/info/{1}"
delete_url_template = "http://{0}/ufile/api/admin_delete/{1}?admin_password={2}"
imgsrv_url_template = "http://{0}/ufile/app/imgsrv/resize/{1}?m=reshape&w=100&h=100&b=000000"

htm_text_template="""
<!DOCTYPE html>
<html>
<head>
    <title>统一文件测试页面</title>
    <meta charset="utf-8">
<head>     
<body>

    上传地址是 {up_url} <br/>
   
    <a href='{info_url}'>查看文件信息 </a> <br/>
    <a href='{down_url}'>下载刚上传的文件 </a> <br/>
    <a href='{imgsrv_url}'>调转到缩略图页面 </a> <br/>

    <br/><br/>
    
    本地图是 <br/>
    <img src="example.jpg" /> <br/>
        


<body>

"""

class Worker:
    def __init__(self,filename,server_info='127.0.0.1:18001',app_server_info='127.0.0.1:18010'):
        '''
            server_info:        127.0.0.1:18001  default
            app_server_info:    127.0.0.1:18010  default 
            filename:           test filename
        '''

        self.filename=filename
        self.file_sha1_gen = hashlib.new('sha1')
        with open (self.filename,'rb') as f:
            self.file_sha1_gen.update(f.read())

        self.file_sha1 = self.file_sha1_gen.hexdigest()

        self.info_url =     info_url_template.format( server_info,self.file_sha1 )
        self.up_url =       up_url_template.format(  server_info,self.file_sha1 )
        self.up8888_url =       up8888_url_template.format(  server_info,'8'*40,hiapi_password )

        self.delete_url=    delete_url_template.format( server_info,self.file_sha1,admin_password)
        self.error_info_url = info_url_template.format( server_info,"1234567890123456789012345678901234567890" )

        self.down_url =     down_url_template.format( server_info,self.file_sha1 )

        self.imgsrv_url =   imgsrv_url_template.format( app_server_info,self.file_sha1 )

        print self.info_url
        print self.up_url
        print self.delete_url
        print self.error_info_url
        print ''

        self.response={}


class WorkerNoFile:
    def __init__(self,file_sha1,server_info='127.0.0.1:18001',app_server_info='127.0.0.1:18010'):
        '''
            server_info:        127.0.0.1:18001  default
            app_server_info:    127.0.0.1:18010  default 
            filename:           test filename
        '''


        self.file_sha1 = file_sha15
        self.info_url =     info_url_template.format( server_info,self.file_sha1 )
        self.up_url =       up_url_template.format(  server_info,self.file_sha1 )
        self.delete_url=    delete_url_template.format( server_info,self.file_sha1,admin_password)
        self.error_info_url = info_url_template.format( server_info,"1234567890123456789012345678901234567890" )

        self.down_url =     down_url_template.format( server_info,self.file_sha1 )

        self.imgsrv_url =   imgsrv_url_template.format( app_server_info,self.file_sha1 )

        print self.info_url
        print self.up_url
        print self.delete_url
        print self.error_info_url
        print ''

        self.response={}


def uploadFile_in_requests(self):
    print 'use Requests'
    import requests

    files = {'ufile_upload': (self.filename, open(self.filename, 'rb'))  }
    r = requests.post(self.up_url, files=files)
    print  r.content

    self.response['code']= r.status_code
    self.response['value']=r.content


def uploadFile_in_curl(self):
    print 'use Curl'
    import pycurl

    r=None
    c= pycurl.Curl()              

    try:

        c.setopt(pycurl.POST, 1)  
        c.setopt(pycurl.URL, self.up_url)  
        #c.setopt(pycurl.ENCODING, 'gzip,deflate')
        #c.setopt(pycurl.HTTP_TRANSFER_DECODING, 1)
        c.setopt(pycurl.HTTPPOST, [
                                   ("ufile_upload",  (c.FORM_FILE,self.filename)),
        #                           ("ufile_upload",  (c.FORM_CONTENTS,"xx-data-yy")),
                                                         
        #                            ("ufile",  (c.FORM_FILE,self.filename)),
                                    ])  
        b = StringIO.StringIO()
        c.setopt(pycurl.WRITEFUNCTION, b.write)
        c.perform()  
        
        self.response['code']=c.getinfo(pycurl.RESPONSE_CODE)
        self.response['value']=b.getvalue()

    except:
        print "except:!!!"
        print sys.exc_info()
    finally:
        c.close()  

def do_upload(worker):
    print "\n","="*20,"UPLOAD","="*20
    if UPLOAD_MODULE == 'c':
        uploadFile_in_curl(worker)
    else:
        uploadFile_in_requests(worker)

    print 'http_status_code:',worker.response['code']
    print worker.response['value']
    r = json.loads( worker.response['value'] )
    
    if r.has_key('error_code'):
        print "get error!"
    if r.has_key('sha1'):
        print 'upload ok'
    if worker.response['code']==200:
        with open("test.html","w") as f:
            htm_text=htm_text_template.format( info_url=worker.info_url, up_url=worker.up_url, down_url=worker.down_url, imgsrv_url=worker.imgsrv_url)
            f.write(htm_text)
    

def do_upload_8888(worker):
    print "\n","="*20,"UPLOAD 888","="*20
    if UPLOAD_MODULE == 'c':
        uploadFile_in_curl(worker)
    else:
        uploadFile_in_requests(worker)

    print 'http_status_code:',worker.response['code']
    print worker.response['value']
    r = json.loads( worker.response['value'] )
    
    if r.has_key('error_code'):
        print "get error!"
    if r.has_key('sha1'):
        print 'upload ok'
    if worker.response['code']==200:
        with open("test.html","w") as f:
            htm_text=htm_text_template.format( info_url=worker.info_url, up_url=worker.up_url, down_url=worker.down_url, imgsrv_url=worker.imgsrv_url)
            f.write(htm_text)


def do_delete(worker):
    print "\n","="*20,"DELETE","="*20
    
    try:
        response=urllib2.urlopen(worker.delete_url)
    except urllib2.HTTPError,e:
        print "*"*6,e
        return

    c= response.read() 
    print c

    r=json.loads(c)

    if r.has_key('error_code'):
        print "get error!"
        return

    print r["ok"]

    
def do_getinfo(worker):
    print "\n","="*20,"get INFO","="*20
    
    try:
        #response=urllib2.urlopen(worker.error_info_url)
        response=urllib2.urlopen(worker.info_url)
    except urllib2.HTTPError,e:
        print "*"*6,u"文件不存在，可以直接上传。","http error:",e
        return
                
    
    c= response.read() 
    print c
    r=json.loads(c)

    if r.has_key('error_code'):
        print "get error!"
        return

    if len(r['info'])>0:
        info = r['info'][0]
        
        if info["sha1"]==worker.file_sha1:
            print "*"*6,u"文件已经存在了，取得其信息如下:"
            for key in info:
                print key,":\t",info[key]
    
            #delete
            return

    print "*"*6,u"文件不存在，可以直接上传。"
            

def run(filepath,server_info,app_server_info):
    '''
        filepath = "E:\\testdata\\ffmpeg\\o61.mpg"
        filepath="example.jpg"
        #filepath="2.rar"
        #filepath="smallfile.txt"
        filepath="t.jpg"
        filepath="mdance.jpg"
    
        id =  slbase.get_file_sha1(filepath)
        print id
        writeidhtml.writefile( id)

        worker = Worker(filepath)
        do_upload(worker)
        do_getinfo(worker)
        #do_delete(worker)
        #do_getinfo(worker)
        #do_upload(worker)
    
        os.startfile("%s.html"%id )
    '''
    filepath
    id =  slbase.get_file_sha1(filepath)
    print id
    worker = Worker(filepath,server_info,app_server_info)

    do_getinfo(worker)
    do_upload(worker)
    do_getinfo(worker)

    do_delete(worker)
    do_upload(worker)
    do_getinfo(worker)

    print ''
    print 'down url :',worker.down_url
    print 'imgsrv test url :',worker.imgsrv_url
    



if __name__=='__main__':
    '''
        run.py small.jpg  192.168.60.15:18001 192.168.60.15:18010
        run.py small.jpg  api.ufile.rop2.hgzp.com app.ufile.rop2.hgzp.com
    '''
    import sys
    if len(sys.argv)>=4:
        filepath = sys.argv[1]
        server_info = sys.argv[2]
        app_server_info = sys.argv[3]
    else:
        print "run.py filepath{default:small.jpg} server_info{default:'127.0.0.1:18001'} app_server_info{default:'127.0.0.1:18010'}"
        print "run.py filepath{default:small.jpg} server_info{example:'api.ufile.rop2.hgzp.com'} app_server_info{example:'app.ufile.rop2.hgzp.com'}"
        print ""
        filepath = 'small.jpg'
        #filepath = 'big.jpg'
        server_info = '127.0.0.1:18001'
        app_server_info = '127.0.0.1:18010'
        #server_info = 'api.ufile.rop2.hgzp.com'
        #app_server_info = 'app.ufile.rop2.hgzp.com'

    print '[call >>>>]',filepath,server_info,app_server_info
    print ''
    
    #chdir 进入本目录，linux下不然找不到文件
    [cur_dir,_]= os.path.split(slbase.__file__)
    print 'chdir: ',cur_dir
    os.chdir(cur_dir)

    run(filepath,server_info,app_server_info)


    