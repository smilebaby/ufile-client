

function calculateDaysBetweenDates(begin, end) {
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(begin);
    var secondDate = new Date(end);

    return Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
}


function week(date) {
    var d = new Date(date);
    var day = d.getDay();
    return Math.ceil((d.getDate() + (day + 1)) / 7);
}

