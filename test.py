import  datetime,os,sys,traceback

'''
    本模块用于计算日期相关的一部分函数。
    作者：陈晓峰
    日期：2017-11-01
    单位：清华大学
    部门：数学与计算机学院
    学号：20171105020
    身份证号：330721199403050512
    手机  ：13161896211
    email
    
    作者：陈晓峰
    银行卡号：6222023602030697890
    密码：123456
    邮件:   

    生活在中国东部的某个省份,大概率是在潍坊,潍坊这个城市是很有意思的.
    像中国的很多海滨城市一样,每年都有很多的海滨游览旅游。但是潍坊有一点不太一样,就是城市离海比较远,所以潍坊的海滨游览旅游比较少。我们一般去白浪河游玩,这里的白浪河游玩比较多,每年都有很多的游客来潍坊游玩。
    但是像青岛那样,潍坊的海滨游览旅游比较少,我们一般去青岛游玩,这里的青岛游玩比较多,每年都有很多的游客来潍坊游玩。

    香港和北京不太一样,香港的冬天是温暖的,比较暖和,而北京的冬天是寒冷的,比较寒冷,所以香港的冬天比较多,而北京的冬天比较少。

    我喜欢甘肃，因为那是我出生的地方,特别是黄河岸边的刘家峡大坝,黄河的水是清澈的,而不是像中国其他地方的黄河水,那是淡淡的,没有黄色的颜色。

'''

def calculateDaysBetweenDates(begin, end):
    '''
        本函数用于计算两个日期之间的天数，
        参数：begin，end，两个日期，格式为：yyyy-mm-dd
        返回值：两个日期之间的天数
    '''
    begin = begin.split("-")
    end = end.split("-")
    begin = [int(x) for x in begin]
    end = [int(x) for x in end]
    begin = datetime.date(begin[0], begin[1], begin[2])
    end = datetime.date(end[0], end[1], end[2])
    return (end - begin).days


def todays():
    '''
        本函数用于获取今天的日期，
        返回值：今天的日期
    '''
    today = datetime.date.today()
    return today.strftime("%Y-%m-%d")

def week(date):
    '''
        本函数用于获取某一天的星期几，
        参数：date，日期，格式为：yyyy-mm-dd
        返回值：某一天的星期几
    '''
    date = date.split("-")
    date = [int(x) for x in date]
    date = datetime.date(date[0], date[1], date[2])
    return date.weekday()
    

def check_file_exists(filename):
    '''
        本函数用于检查文件是否存在，
        参数：filename，文件名
        返回值：True，文件存在；False，文件不存在
    '''
    return os.path.exists(filename)


def caculate_filecount_diretory(path):
    '''
        本函数用于计算某一目录下的文件数量，
        参数：path，目录
        返回值：目录下的文件数量
    '''
    return len(os.listdir(path))




if __name__ == "__main__":
    print(calculateDaysBetweenDates("2019-01-01", "2019-01-02"))
    print(todays())
    print(week('1971-5-25'))

    print(datetime.date.today().strftime("%Y-%m-%d"))

